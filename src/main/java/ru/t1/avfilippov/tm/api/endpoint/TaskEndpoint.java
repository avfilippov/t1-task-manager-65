package ru.t1.avfilippov.tm.api.endpoint;

import ru.t1.avfilippov.tm.dto.TaskDto;

import java.util.List;

public interface TaskEndpoint {

    List<TaskDto> findAll();

    TaskDto save(TaskDto task);

    TaskDto findById(String id);

    boolean existsById(String id);

    long count();

    void deleteById(String id);

    void deleteById(TaskDto task);

    void deleteById(List<TaskDto> tasks);

    void clear();

}
