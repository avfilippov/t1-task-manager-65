package ru.t1.avfilippov.tm.api.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.avfilippov.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectDTOService {

    List<ProjectDto> findAll();

    ProjectDto save(@RequestBody ProjectDto project);

    ProjectDto save();

    ProjectDto findById(@PathVariable("id") String id);

    boolean existsById(@PathVariable("id") String id);

    long count();

    void deleteById(@PathVariable("id") String id);

    void delete(@RequestBody ProjectDto project);

    void deleteAll(@RequestBody List<ProjectDto> projects);

    void clear();

}
