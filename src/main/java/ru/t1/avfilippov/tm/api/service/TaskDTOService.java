package ru.t1.avfilippov.tm.api.service;

import ru.t1.avfilippov.tm.dto.TaskDto;

import java.util.List;

public interface TaskDTOService {

    List<TaskDto> findAll();

    TaskDto save(TaskDto task);

    TaskDto save();

    TaskDto findById(String id);

    boolean existsById(String id);

    long count();

    void deleteById(String id);

    void delete(TaskDto task);

    void deleteAll(List<TaskDto> tasks);

    void clear();

}
