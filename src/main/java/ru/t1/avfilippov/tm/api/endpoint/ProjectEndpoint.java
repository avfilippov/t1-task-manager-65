package ru.t1.avfilippov.tm.api.endpoint;

import ru.t1.avfilippov.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectEndpoint {

    List<ProjectDto> findAll();

    ProjectDto save(ProjectDto project);

    ProjectDto findById(String id);

    boolean existsById(String id);

    long count();

    void deleteById(String id);

    void deleteById(ProjectDto project);

    void deleteById(List<ProjectDto> projects);

    void clear();

}
